public class Contact {

    private String name;
    private String contactNumber;
    private String Address;

    public Contact(){

    }

    public Contact(String name, String contactNumber, String Address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.Address = Address;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return Address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

}
