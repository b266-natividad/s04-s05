public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Jd Natividad", "639-123-4562", "241 Liyue St.");

        Contact contact2 = new Contact("Alexander Hamilton", "123-456-7870", "123 Somewhere st.");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()){
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println();
            }
        }
    }
}
