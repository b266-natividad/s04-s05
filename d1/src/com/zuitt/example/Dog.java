package com.zuitt.example;

public class Dog extends Animal {
    private String breed;

    public Dog(){
        super();/*Animal Constructor*/
        this.breed = "maltese";

    }

    public Dog(String name, String color, String breed){
        super(name, color);/*Animal(String name, String color) -> constructor*/
        this.breed = breed;


    }

    /*Getters*/
     public String getBreed(){
         return this.breed;
     }

     /*methods*/
    public void speak(){
        super.call();/*The call() method from Animal Class*/
        System.out.println("woof woof!");
    }

}
